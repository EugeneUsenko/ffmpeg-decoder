/*
 * utils.c
 *
 *  Created on: Apr 6, 2013
 *      Author: ievgen usenko
 */

#include "thread_utils.h"
#include <assert.h>
#include <stdlib.h>

pthread_mutex_t* create_mutex() {
    pthread_mutex_t* mutex = (pthread_mutex_t*)malloc(sizeof(pthread_mutex_t));
    if (mutex == NULL) {
        return NULL;
    }

    if (pthread_mutex_init(mutex, NULL)) {
        free((void*)mutex);
        return NULL;
    }

    return mutex;
}

void destroy_mutex(pthread_mutex_t* mutex) {
    assert(mutex!=NULL);

    pthread_mutex_destroy(mutex);
    free((void*)mutex);
}

pthread_cond_t* create_cond() {
    pthread_cond_t* cond = (pthread_cond_t*)malloc(sizeof(pthread_cond_t));
    if (cond == NULL) {
        return NULL;
    }

    if (pthread_cond_init(cond, NULL)) {
        free((void*)cond);
        return NULL;
    }
    return cond;
}

void destroy_cond(pthread_cond_t* cond) {
    assert(cond!=NULL);

    pthread_cond_destroy(cond);
    free((void*)cond);
}

void wait(pthread_mutex_t* mutex, pthread_cond_t* cond) {
    assert(mutex != NULL);
    assert(cond != NULL);

    pthread_mutex_lock(mutex);
    pthread_cond_wait(cond, mutex);
    pthread_mutex_unlock(mutex);
}

void notify(pthread_mutex_t* mutex, pthread_cond_t* cond) {
    assert(mutex != NULL);
    assert(cond != NULL);

    pthread_mutex_lock(mutex);
    pthread_cond_signal(cond);
    pthread_mutex_unlock(mutex);
}

