#include "decoder.h"

/** Returns readable error message for specific ffmpeg error code. */
static const char* decoder_get_cause(const int);

static int decoder_initialize_threads(DecoderContext*);
//static int decoder_initialize_thread(pthread_mutex_t*, pthread_cond_t*);
static void decoder_stop_threads(DecoderContext*);
static void decoder_stop_thread(pthread_t, pthread_mutex_t*, pthread_cond_t*);

static int decoder_initialize_queues(DecoderContext*);
static int decoder_open_url(DecoderContext*, DecoderParameters*);
static int decoder_find_stream_info(DecoderContext*);
static int decoder_load_codecs(DecoderContext*);
static int decoder_open_codec(AVCodecContext *codec_context);
static int decoder_setup_video_frame(DecoderContext*, int, int);
static int decoder_setup_audio_frame(DecoderContext*);
static int decoder_start_threads(DecoderContext*);

static void decoder_cleanup(DecoderContext*);

static void* decoder_video_thread(void*);
static void* decoder_audio_thread(void*);
static void* decoder_reading_thread(void*);

static void decoder_thread_notify(pthread_mutex_t*, pthread_cond_t*);
static void decoder_thread_wait(pthread_mutex_t*, pthread_cond_t*);

static void decoder_fill_video_frame(DecoderContext* decoder);
static void decoder_fill_audio_frame(DecoderContext* decoder);

#define VIDOE_QUEUE_SIZE 20
#define AUDIO_QUEUE_SIZE 20

DecoderContext* decoder_start(DecoderParameters* parameters, void (*video_frame_callback)(uint8_t*, uint32_t),
		void (*audio_frame_callback)(uint8_t*, uint32_t)) {
	av_log(NULL, AV_LOG_INFO, "decoder_start()...\n\n");
	DecoderContext* decoder = (DecoderContext*) malloc(sizeof(DecoderContext));
	if (!decoder) {
		av_log(NULL, AV_LOG_ERROR, "Can't allocate memory for decoder context\n\n\n");
		return NULL;
	}
	memset(decoder, 0, sizeof(DecoderContext));
	decoder->video.callback = video_frame_callback;
	decoder->audio.callback = audio_frame_callback;

	av_register_all();
	avformat_network_init();

	if (decoder_initialize_threads(decoder))
		goto deinit;

	if (decoder_initialize_queues(decoder))
		goto deinit;

	if (decoder_open_url(decoder, parameters))
		goto deinit;

	if (decoder_find_stream_info(decoder))
		goto cleanup;

	if (decoder_load_codecs(decoder))
		goto cleanup;

	if (decoder_setup_video_frame(decoder, parameters->video_width, parameters->video_height))
		goto cleanup;

	if (decoder_setup_audio_frame(decoder))
		goto cleanup;

	if (decoder_start_threads(decoder))
		goto cleanup;

	return decoder;

	cleanup: decoder_cleanup(decoder);
	deinit: avformat_network_deinit();
	return NULL;

}

void decoder_stop(DecoderContext* decoder) {
	av_log(NULL, AV_LOG_INFO, "decoder_stop()...\n\n");
	decoder_stop_threads(decoder);
	queue_release(&decoder->video.stream.packets);
	queue_release(&decoder->audio.stream.packets);
	decoder_cleanup(decoder);
	free(decoder);
}

int decoder_initialize_threads(DecoderContext* decoder) {
	av_log(NULL, AV_LOG_INFO, "decoder_initialize_threads()...\n\n");

	decoder->video.mutex = malloc(sizeof(pthread_mutex_t));
	decoder->video.cond = malloc(sizeof(pthread_cond_t));
	pthread_mutex_init(decoder->video.mutex, NULL);
	pthread_cond_init(decoder->video.cond, NULL);

	decoder->audio.mutex = malloc(sizeof(pthread_mutex_t));
	decoder->audio.cond = malloc(sizeof(pthread_cond_t));
	pthread_mutex_init(decoder->audio.mutex, NULL);
	pthread_cond_init(decoder->audio.cond, NULL);

//    if (decoder_initialize_thread(decoder->video.mutex, decoder->video.cond)) {
//        av_log(NULL, AV_LOG_ERROR, "decoder_initialize_threads() - can't initialize video threads\n\n");
//        return -1;
//    }
//
//    if (decoder_initialize_thread(decoder->audio.mutex, decoder->audio.cond)) {
//        av_log(NULL, AV_LOG_ERROR, "decoder_initialize_threads() - can't initialize video threads\n\n");
//        return -1;
//    }

	return 0;
}

//int decoder_initialize_thread(pthread_mutex_t* mutex, pthread_cond_t* cond) {
//    av_log(NULL, AV_LOG_INFO, "decoder_initialize_thread()...\n\n");
//    mutex = malloc(sizeof(pthread_mutex_t));
//    if (!mutex) {
//        return -1;
//    }
//    if (pthread_mutex_init(mutex, NULL))
//        goto release_mutex;
//
//    cond = malloc(sizeof(pthread_cond_t));
//    if (!cond) {
//        goto destroy_mutex;
//    }
//    if (pthread_cond_init(cond, NULL))
//        goto release_cond;
//
//    return 0;
//
//    release_cond: free(cond);
//    destroy_mutex: pthread_mutex_destroy(mutex);
//    release_mutex: free(mutex);
//    mutex = NULL;
//    cond = NULL;
//
//    return -1;
//}

int decoder_initialize_queues(DecoderContext* decoder) {
	av_log(NULL, AV_LOG_INFO, "decoder_initiliaze_queues()...\n\n");

	if (queue_init(&decoder->video.stream.packets, VIDOE_QUEUE_SIZE)) {
		av_log(NULL, AV_LOG_ERROR, "decoder_initiliaze_queues() - can't initialize video queue\n\n");
		return -1;
	}

	if (queue_init(&decoder->audio.stream.packets, AUDIO_QUEUE_SIZE)) {
		av_log(NULL, AV_LOG_ERROR, "decoder_initiliaze_queues() - can't initialize audio queue\n\n");
		return -1;
	}
	return 0;
}

int decoder_open_url(DecoderContext* decoder, DecoderParameters* parameters) {
	av_log(NULL, AV_LOG_INFO, "decoder_open_url()...\n\n");
	int rc;

	AVDictionary *opts = NULL;
	av_dict_set(&opts, "rtsp_transport", "tcp", 0);
	if ((rc = avformat_open_input(&decoder->format, parameters->url, NULL, NULL)) < 0) {
		av_log(NULL, AV_LOG_ERROR, "decoder_open_url() - %s\n", decoder_get_cause(rc));
		return -1;
	}

	if (opts) {
		av_dict_free(&opts);
	}

	return 0;
}

int decoder_find_stream_info(DecoderContext* decoder) {
	av_log(NULL, AV_LOG_INFO, "decoder_find_stream_info()...\n\n");
	int rc;
	if ((rc = avformat_find_stream_info(decoder->format, NULL)) < 0) {
		av_log(NULL, AV_LOG_ERROR, "decoder_find_stream_info() - %s\n", decoder_get_cause(rc));
		return -1;
	}
	return 0;
}

int decoder_load_codecs(DecoderContext* decoder) {
	av_log(NULL, AV_LOG_INFO, "decoder_load_codecs()...\n\n");
	int i;
	for (i = 0; i < decoder->format->nb_streams; i++) {
		int type = decoder->format->streams[i]->codec->codec_type; //AVMEDIA_TYPE_VIDEO/AVMEDIA_TYPE_AUDIO
//        int id = decoder->format->streams[i]->codec->codec_id; AV_CODEC_ID_H264/AV_CODEC_ID_AAC
		if (type == AVMEDIA_TYPE_VIDEO) {
			av_log(NULL, AV_LOG_INFO, "decoder_load_codecs() - found video stream: index=%d\n", i);
			decoder->video.stream.index = i;
			decoder->video.stream.codec = decoder->format->streams[i]->codec;
			if (decoder_open_codec(decoder->video.stream.codec) < 0) {
				return -1;
			}
		} else if (type == AVMEDIA_TYPE_AUDIO) {
			av_log(NULL, AV_LOG_INFO, "decoder_load_codecs() - found audio stream: index=%d\n", i);
			decoder->audio.stream.index = i;
			decoder->audio.stream.codec = decoder->format->streams[i]->codec;
			decoder_open_codec(decoder->audio.stream.codec);
		}
	}
	return 0;
}

int decoder_open_codec(AVCodecContext *codec_context) {
	av_log(NULL, AV_LOG_INFO, "decoder_open_codec()...\n\n");
	AVCodec *codec = avcodec_find_decoder(codec_context->codec_id);
	if (codec == NULL) {
		av_log(NULL, AV_LOG_ERROR, "decoder_open_codec() - %s codec was not found.\n", codec_context->codec->name);
		return -1;
	}

	if (avcodec_open2(codec_context, codec, NULL) < 0) {
		av_log(NULL, AV_LOG_ERROR, "decoder_open_codec() - could not open %s codec.\n", codec_context->codec->name);
		return -1;
	}
	return 0;
}

int decoder_setup_video_frame(DecoderContext* decoder, int outputWidth, int outputHeight) {
	av_log(NULL, AV_LOG_INFO, "decoder_setup_video_frame()...\n\n");
	int srcWidth = decoder->video.stream.codec->width;
	int srcHeight = decoder->video.stream.codec->height;
	int srcFormat = decoder->video.stream.codec->pix_fmt;

	int dstWidth = outputWidth;
	int dstHeight = outputHeight;
	int dstFormat = PIX_FMT_RGB565LE;

	decoder->video.yuv = avcodec_alloc_frame();
	if (!decoder->video.yuv) {
		av_log(NULL, AV_LOG_ERROR, "decoder_setup_video_frame() - can't allocate YUV frame\n\n");
		return -1;
	}

	decoder->video.rgb = avcodec_alloc_frame();
	if (!decoder->video.rgb) {
		av_log(NULL, AV_LOG_ERROR, "decoder_setup_video_frame() - can't allocate RGB frame\n\n");
		goto free_yuv;
	}

	decoder->video.frame_size = avpicture_get_size(dstFormat, dstWidth, dstHeight);
	decoder->video.frame = (uint8_t*) av_malloc(decoder->video.frame_size * sizeof(uint8_t));
	if (!decoder->video.frame) {
		av_log(NULL, AV_LOG_ERROR, "decoder_setup_video_frame() - can't allocate memory for RGB\n\n");
		goto free_rgb;
	}

	decoder->sws = sws_getContext(srcWidth, srcHeight, srcFormat, dstWidth, dstHeight, dstFormat, SWS_FAST_BILINEAR,
			NULL, NULL, NULL);
	if (!decoder->sws) {
		av_log(NULL, AV_LOG_ERROR, "decoder_setup_video_frame() - can't allocate SWS context\n\n");
		goto free_rgb_buffer;
	}

	avpicture_fill((AVPicture*) decoder->video.rgb, decoder->video.frame, PIX_FMT_RGB565LE, dstWidth, dstHeight);
	return 0;

	free_rgb_buffer: free(decoder->video.frame);
	free_rgb: av_free(decoder->video.rgb);
	free_yuv: av_free(decoder->video.yuv);
	decoder->video.rgb = NULL;
	decoder->video.yuv = NULL;
	decoder->video.frame = NULL;
	return -1;
}

int decoder_setup_audio_frame(DecoderContext* decoder) {
	av_log(NULL, AV_LOG_INFO, "decoder_setup_audio_frame()...\n\n");
	int size = AVCODEC_MAX_AUDIO_FRAME_SIZE * 3 / 2;

	decoder->audio.frame = av_malloc(size);
	if (!decoder->audio.frame) {
		av_log(NULL, AV_LOG_ERROR, "decoder_setup_audio_frame() - audio frame refer to NULL pointer\n\n");
		return -1;
	}
	decoder->audio.frame_size = size;

	av_log(NULL, AV_LOG_INFO, "decoder_setup_audio_frame() - allocated %d bytes for audio frame", size);
	return 0;
}

void decoder_cleanup(DecoderContext* decoder) {
	av_log(NULL, AV_LOG_INFO, "decoder_cleanup()...\n\n");

	if (decoder->sws) {
		sws_freeContext(decoder->sws);
		decoder->sws = NULL;
	}

	if (decoder->audio.stream.codec) {
		avcodec_close(decoder->audio.stream.codec);
		decoder->audio.stream.codec = NULL;
	}

	if (decoder->video.stream.codec) {
		avcodec_close(decoder->video.stream.codec);
		decoder->video.stream.codec = NULL;
	}

	if (decoder->format) {
		avformat_close_input(&decoder->format);
		decoder->format = NULL;
	}

	if (decoder->video.yuv) {
		av_free(decoder->video.yuv);
		decoder->video.yuv = NULL;
	}

	if (decoder->video.rgb) {
		av_free(decoder->video.rgb);
		decoder->video.rgb = NULL;
	}

	if (decoder->audio.frame) {
		av_free(decoder->audio.frame);
		decoder->audio.frame = NULL;
	}

	avformat_network_deinit();
}

const char* decoder_get_cause(const int errnum) {
	static char msg[1024];
	av_strerror(errnum, msg, sizeof(msg));
	return msg;
}

int decoder_start_threads(DecoderContext* decoder) {
	av_log(NULL, AV_LOG_INFO, "decoder_start_threads()...\n\n");

	decoder->running = DECODER_IS_RUNNING;

	if (pthread_create(&decoder->video.thread, NULL, decoder_video_thread, (void*) decoder)) {
		av_log(NULL, AV_LOG_ERROR, "decoder_start_threads() - can't start audio thread\n\n");
		return -1;
	}

	if (pthread_create(&decoder->audio.thread, NULL, decoder_audio_thread, (void*) decoder)) {
		av_log(NULL, AV_LOG_ERROR, "decoder_start_threads() - can't start audio thread\n\n");
		decoder->running = DECODER_IS_STOPPED;
		decoder_stop_thread(decoder->video.thread, decoder->video.mutex, decoder->video.cond);
		return -1;
	}

	if (pthread_create(&decoder->reading_thread, NULL, decoder_reading_thread, (void*) decoder)) {
		av_log(NULL, AV_LOG_ERROR, "decoder_start_threads() - can't reading audio thread\n\n");
		decoder->running = DECODER_IS_STOPPED;
		decoder_stop_thread(decoder->audio.thread, decoder->audio.mutex, decoder->audio.cond);
		decoder_stop_thread(decoder->video.thread, decoder->video.mutex, decoder->video.cond);
		return -1;
	}

	return 0;
}

void decoder_stop_threads(DecoderContext* decoder) {
	av_log(NULL, AV_LOG_INFO, "decoder_stop_threads()...\n\n");
	decoder->running = DECODER_IS_STOPPED;

	if (decoder->reading_thread) {
		pthread_join(decoder->reading_thread, NULL);
		decoder->reading_thread = 0;
	}

	if (decoder->video.thread) {
		decoder_stop_thread(decoder->video.thread, decoder->video.mutex, decoder->video.cond);
		decoder->video.mutex = NULL;
		decoder->video.cond = NULL;
		decoder->video.thread = 0;
	}

	if (decoder->audio.thread) {
		decoder_stop_thread(decoder->audio.thread, decoder->audio.mutex, decoder->audio.cond);
		decoder->audio.mutex = NULL;
		decoder->audio.cond = NULL;
		decoder->audio.thread = 0;
	}
}

static void decoder_stop_thread(pthread_t th, pthread_mutex_t* mutex, pthread_cond_t* cond) {
	decoder_thread_notify(mutex, cond);
	pthread_join(th, NULL);
	pthread_cond_destroy(cond);
	pthread_mutex_destroy(mutex);
	free(mutex);
	free(cond);
}

void decoder_thread_notify(pthread_mutex_t* mutex, pthread_cond_t* cond) {
//    av_log(NULL, AV_LOG_INFO, "decoder_thread_notify()...\n\n");
	pthread_mutex_lock(mutex);
	pthread_cond_signal(cond);
	pthread_mutex_unlock(mutex);
}

void decoder_thread_wait(pthread_mutex_t* mutex, pthread_cond_t* cond) {
//    av_log(NULL, AV_LOG_INFO, "decoder_thread_wait()...\n\n");
	pthread_mutex_lock(mutex);
	pthread_cond_wait(cond, mutex);
	pthread_mutex_unlock(mutex);
}

void* decoder_video_thread(void* ptr) {
	av_log(NULL, AV_LOG_INFO, "decoder_video_thread() - has been started\n\n");
	DecoderContext* decoder = (DecoderContext*) ptr;

	while (decoder->running) {
		if (decoder->video.stream.packets.nb_packets == 0) {
			decoder_thread_wait(decoder->video.mutex, decoder->video.cond);
		}
		decoder_fill_video_frame(decoder);
	}
	av_log(NULL, AV_LOG_INFO, "decoder_video_thread() - has been stopped\n\n");
	return NULL;
}

void* decoder_audio_thread(void* ptr) {
	av_log(NULL, AV_LOG_INFO, "decoder_audio_thread() - has been started\n\n");
	DecoderContext* decoder = (DecoderContext*) ptr;

	while (decoder->running) {
		if (decoder->audio.stream.packets.nb_packets == 0) {
			decoder_thread_wait(decoder->audio.mutex, decoder->audio.cond);
		}
		decoder_fill_audio_frame(decoder);

	}
	av_log(NULL, AV_LOG_INFO, "decoder_audio_thread() - has been stopped\n\n");
	return NULL;
}

void* decoder_reading_thread(void* ptr) {
	av_log(NULL, AV_LOG_INFO, "decoder_reading_thread() - has been started\n\n");

	DecoderContext* decoder = (DecoderContext*) ptr;
	while (decoder->running) {
		AVPacket packet;

		if (av_read_frame(decoder->format, &packet) >= 0) {
			if (packet.stream_index == decoder->video.stream.index) {
				queue_put(&decoder->video.stream.packets, &packet);
				decoder_thread_notify(decoder->video.mutex, decoder->video.cond);
			} else if (packet.stream_index == decoder->audio.stream.index) {
				queue_put(&decoder->audio.stream.packets, &packet);
				decoder_thread_notify(decoder->audio.mutex, decoder->audio.cond);
			} else {
				av_log(NULL, AV_LOG_ERROR, "decoder_reading_thread() - unknown packet type, index:%d",
						packet.stream_index);
			}
		}
	}

	av_log(NULL, AV_LOG_INFO, "decoder_reading_thread() - has been stopped\n\n");
	return NULL;
}

void decoder_fill_video_frame(DecoderContext* decoder) {
	AVPacket packet;
	queue_get(&decoder->video.stream.packets, &packet);
	if (packet.data) {
		int finished;
		if (avcodec_decode_video2(decoder->video.stream.codec, (AVFrame*) decoder->video.yuv, &finished, &packet) > 0) {
			sws_scale(decoder->sws, (const uint8_t * const *) decoder->video.yuv->data, decoder->video.yuv->linesize, 0,
					decoder->video.stream.codec->height, decoder->video.rgb->data, decoder->video.rgb->linesize);
			decoder->video.callback(decoder->video.frame, decoder->video.frame_size);
		}
		av_free_packet(&packet);
	}
}

void decoder_fill_audio_frame(DecoderContext* decoder) {
	AVPacket packet;
	int len = decoder->audio.frame_size;
	queue_get(&decoder->audio.stream.packets, &packet);
	if (packet.data) {
		int datalen = avcodec_decode_audio3(decoder->audio.stream.codec, decoder->audio.frame, &len, &packet);
		if (datalen > 0) {
			decoder->audio.callback(decoder->audio.frame, datalen);
		}
		av_free_packet(&packet);
	}
}

