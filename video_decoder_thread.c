/*
 * VideoDecoderThread.c
 *
 *  Created on: Apr 6, 2013
 *      Author: ievgen usenko
 */
#include <assert.h>

#include "video_decoder_thread.h"
#include "queue.h"
#include "logging.h"
#include "ffmpeg.h"

static void* decoding_thread(void*);
static void decode_packet(AVPacket*, AVCodecContext*);

static PacketQueue* packets = NULL;
static int eof = 0;
static int running = 0;
static pthread_t thread = 0;

static VideoThreadParameters* params;

int video_start(AVCodecContext* ctx) {
    eof = 0;
    running = 1;
    packets = queue_create();
    assert(packets!=NULL);

    if (pthread_create(&thread, NULL, &decoding_thread, (void*) ctx)) {
        queue_release(packets);
        return -1;
    }
    return 0;
}

void video_stop() {
    assert(thread!=0);
    assert(running!=0);
    assert(packets!=NULL);

    running = 0;
    notify(packets->mutex, packets->cond);
    pthread_join(thread, NULL);
    thread = 0;

    queue_release(packets);
    packets = NULL;
}

void video_push_packet(AVPacket* packet) {
    queue_put(packets, packet);
}

void video_eof() {
    eof = 1;
    notify(packets->mutex, packets->cond);
}

void* decoding_thread(void* p) {
    LOGI("Video decoding thread - has been started\n");

    AVCodecContext* ctx = (AVCodecContext*) p;
    AVPacket packet;

    while (running) {
        if (queue_get(packets, &packet)) {
            decode_packet(&packet, ctx);
            av_free_packet(&packet);
            continue;
        }

        if (running == 0 || (eof && packets->nb_packets == 0)) {
            break;
        }
    }

    LOGI("Video decoding thread - has been stopped\n");
    return NULL;
}

void decode_packet(AVPacket* packet, AVCodecContext* ctx) {
    int got_frame;
    avcodec_get_frame_defaults(params->yuv);
    int len = avcodec_decode_video2(ctx, (AVFrame*) params->yuv, &got_frame, packet);
    if (len < 0) {
        LOGE("Failed to decode video frame\n");
        return;
    }
    if (got_frame) {
        sws_scale(params->sws, (const uint8_t * const *) params->yuv->data, params->yuv->linesize, 0, ctx->height,
                params->rgb->data, params->rgb->linesize);
//		params->callback(params->rgb_frame_data, params->rgb_frame_size);
    }
}

VideoThreadParameters* create_vparameters(AVCodecContext* ctx, int outWidth, int outHeight) {
    VideoThreadParameters* params = (VideoThreadParameters*) malloc(sizeof(VideoThreadParameters));
    assert(params!=NULL);

    int srcWidth = ctx->width;
    int srcHeight = ctx->height;
    int srcFormat = ctx->pix_fmt;

    int dstWidth = outWidth;
    int dstHeight = outHeight;
    int dstFormat = PIX_FMT_YUV420P; //PIX_FMT_RGB565LE;

    params->yuv = avcodec_alloc_frame();
    params->rgb = avcodec_alloc_frame();
    int rgb_frame_size = avpicture_get_size(dstFormat, dstWidth, dstHeight);
    uint8_t* rgb_frame_data = (uint8_t*) av_malloc(rgb_frame_size * sizeof(uint8_t));
    params->sws = sws_getContext(srcWidth, srcHeight, srcFormat, dstWidth, dstHeight, dstFormat, SWS_FAST_BILINEAR,
            NULL, NULL, NULL);

    assert(params->yuv!=NULL);
    assert(params->rgb!=NULL);
    assert(params->sws!=NULL);
    assert(rgb_frame_data!=NULL);

    avpicture_fill((AVPicture*) params->rgb, rgb_frame_data, dstFormat, dstWidth, dstHeight);
    return params;
}

void release_vparameters(VideoThreadParameters* params) {
    assert(params!=NULL);
    assert(params->yuv!=NULL);
    assert(params->rgb!=NULL);
    assert(params->sws!=NULL);

    sws_freeContext(params->sws);
    av_free(params->yuv);
    av_free(params->rgb);
}

void video_set_parameters(void* p) {
    assert(p!=NULL);

    params = (VideoThreadParameters*) p;
    assert(params->callback!=NULL);
    assert(params->rgb!=NULL);
    assert(params->yuv!=NULL);
    assert(params->sws!=NULL);
}

