/*
 * logging.h
 *
 *  Created on: Apr 6, 2013
 *      Author: zhenya
 */

#ifndef LOGGING_H_
#define LOGGING_H_

#ifdef __ANDROID__
#define LOG_TAG "ffmpeg.decoder"
#define LOGE(...) {__android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__);}
#define LOGI(...) {__android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__);}
#define LOGD(...) {__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__);}
#else
#define LOGE(...) {av_log(NULL, AV_LOG_ERROR, __VA_ARGS__);}
#define LOGI(...) {av_log(NULL, AV_LOG_INFO, __VA_ARGS__);}
#define LOGD(...) {av_log(NULL, AV_LOG_DEBUG, __VA_ARGS__);}
#endif

#endif /* LOGGING_H_ */
