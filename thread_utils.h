/*
 * utils.h
 *
 *  Created on: Apr 6, 2013
 *      Author: ievgen usenko
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <pthread.h>

pthread_mutex_t* create_mutex();
void destroy_mutex(pthread_mutex_t* mutex);

pthread_cond_t* create_cond();
void destroy_cond(pthread_cond_t* cond);

void wait(pthread_mutex_t*, pthread_cond_t*);
void notify(pthread_mutex_t*, pthread_cond_t*);

#endif /* UTILS_H_ */
