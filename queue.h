#ifndef queue_h
#define queue_h

#include <libavformat/avformat.h>
#include <pthread.h>

typedef struct {
    AVPacketList *first, *last;
    uint8_t nb_packets;
    pthread_mutex_t* mutex;
    pthread_cond_t* cond;

} PacketQueue;

PacketQueue* queue_create();
void queue_release(PacketQueue*);

/** Put packet into queue. Be aware that this method is not thread safe. */
void queue_put(PacketQueue*, AVPacket*);

/** Get packet from queue. Be aware that this method is not thread safe. */
int queue_get(PacketQueue*, AVPacket*);

#endif
