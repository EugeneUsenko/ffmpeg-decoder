/*
 * decoder_context.h
 *
 *  Created on: Apr 6, 2013
 *      Author: ievgen usenko
 */

#ifndef DECODER_CONTEXT_H_
#define DECODER_CONTEXT_H_

#include "ffmpeg.h"

typedef struct {
    AVFormatContext* format;

    AVCodecContext* videoCodec;
    AVCodecContext* audioCodec;

    int videoStreamIndex;
    int audioStreamSndex;

} Context;

typedef struct {
    int  (*start)(AVCodecContext*);
    void (*set_parameters)(void*);
    void (*stop)(void);
    void (*push)(AVPacket*);
    void (*eof)(void);
} DecodingThread;

Context* open_decoder_context(const char* url);
void close_decoder_context(Context*);

#endif /* DECODER_CONTEXT_H_ */
