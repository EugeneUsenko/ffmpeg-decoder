/*
 * decoder_context.c
 *
 *  Created on: Apr 6, 2013
 *      Author: ievgen usenko
 */

#include <assert.h>
#include "decoder_context.h"
#include "logging.h"

static int open_url(const char* url, Context* ctx);
static int find_stream_info(Context* ctx);
static int load_codecs(Context* ctx);
static int open_codec(AVCodecContext *codexCtx);
static char* get_strerror(const int errnum);

Context* open_decoder_context(const char* url) {
    assert(url!=NULL);

    Context* ctx = (Context*) malloc(sizeof(Context));
    if (ctx == NULL) {
        LOGE("Can't allocate memory for context\n");
        return NULL;
    }

    memset(ctx, 0, sizeof(Context));
    ctx->videoStreamIndex = -1;
    ctx->audioStreamSndex = -1;

    av_register_all();
    avformat_network_init();

    if (open_url(url, ctx)) {
        free(ctx);
        return NULL;
    }

    if (find_stream_info(ctx)) {
        close_decoder_context(ctx);
        return NULL;
    }

    if (load_codecs(ctx)) {
        close_decoder_context(ctx);
        return NULL;
    }

    return ctx;
}

void close_decoder_context(Context* ctx) {
    assert(ctx != NULL);
    assert(ctx->format != NULL);

    if (ctx->audioCodec != NULL) {
        avcodec_close(ctx->audioCodec);
        av_free(ctx->audioCodec);
    }

    if (ctx->videoCodec != NULL) {
        avcodec_close(ctx->videoCodec);
        av_free(ctx->videoCodec);
    }
//    avformat_close_input(ctx->format);
    free(ctx);
}

int open_url(const char* url, Context* ctx) {
    AVDictionary *opts = NULL;
    int rc = 0;

    av_dict_set(&opts, "rtsp_transport", "tcp", 0);
    if ((rc = avformat_open_input(&ctx->format, url, NULL, &opts)) != 0) {
        LOGE("avformat_open_input() - %s\n", get_strerror(rc));
    }

    if (opts) {
        av_dict_free(&opts);
    }
    return rc;
}

int find_stream_info(Context* ctx) {
    int rc;
    if ((rc = avformat_find_stream_info(ctx->format, NULL)) < 0) {
        LOGE("avformat_find_stream_info() - %s\n", get_strerror(rc));
        return -1;
    }
    return 0;
}

int load_codecs(Context* ctx) {
    int i;
    for (i = 0; i < ctx->format->nb_streams; i++) {
        int id = ctx->format->streams[i]->codec->codec_id;
        int type = ctx->format->streams[i]->codec->codec_type;

        if (ctx->audioStreamSndex != -1 && ctx->videoStreamIndex != -1) {
            break;
        }

        if (type == AVMEDIA_TYPE_VIDEO) {
            ctx->videoStreamIndex = i;
            ctx->videoCodec = ctx->format->streams[i]->codec;
            if (open_codec(ctx->videoCodec)) {
                LOGE("Can't open codec for video stream: index=%d\n", i);
                continue;
            }
            LOGI("Found video stream: index=%d, codec=%s\n", i, ctx->videoCodec->codec->name);
        } else if (type == AVMEDIA_TYPE_AUDIO) {
            ctx->audioStreamSndex = i;
            ctx->audioCodec = ctx->format->streams[i]->codec;
            if (open_codec(ctx->audioCodec)) {
                LOGE("Can't open codec for audio stream: index=%d\n", i);
                continue;
            }
            LOGI("Found audio stream: index=%d, codec=%s\n", i, ctx->audioCodec->codec->name);
        }
    }

    if (ctx->videoStreamIndex == -1) {
        LOGE("Video stream has found\n");
        return -1;
    }

    if (ctx->audioStreamSndex == -1) {
        LOGI("Audio stream has found\n");
    }

    return 0;
}

int open_codec(AVCodecContext *codexCtx) {
    AVCodec *codec = avcodec_find_decoder(codexCtx->codec_id);
    if (codec == NULL) {
//
        return -1;
    }

    if (avcodec_open2(codexCtx, codec, NULL) < 0) {
//        LOGE("avcodec_open2() - could not open %s c.\n", ctx->codec->name);
        return -1;
    }
    return 0;
}

char* get_strerror(const int errnum) {
    static char msg[1024];
    av_strerror(errnum, msg, sizeof(msg));
    return msg;
}
