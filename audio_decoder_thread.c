/*
 * audio_decoder_thread.c
 *
 *  Created on: Apr 6, 2013
 *      Author: ievgen usenko
 */

#include <assert.h>
#include "ffmpeg.h"
#include "queue.h"
#include "logging.h"

static void* decoding_thread(void*);
static void decode_packet(AVPacket*, AVCodecContext*);

static PacketQueue* packets = NULL;
static int eof = 0;
static int running = 0;
static pthread_t thread = 0;

int audio_start(AVCodecContext* ctx) {
    running = 1;
    eof = 0;
    packets = queue_create();
    assert(packets!=NULL);

    if (pthread_create(&thread, NULL, &decoding_thread, (void*) ctx)) {
        queue_release(packets);
        return -1;
    }
    return 0;
}

void audio_stop() {
    assert(thread!=0);
    assert(running!=0);
    assert(packets!=NULL);

    running = 0;
    notify(packets->mutex, packets->cond);
    pthread_join(thread, NULL);
    thread = 0;

    queue_release(packets);
    packets = NULL;
}

void audio_push_packet(AVPacket* packet) {
    queue_put(packets, packet);
    notify(packets->mutex, packets->cond);
}

void audio_eof() {
    eof = 1;
    notify(packets->mutex, packets->cond);
}

void* decoding_thread(void* p) {
    LOGI("Audio decoding thread - has been started\n");

    AVCodecContext* ctx = (AVCodecContext*) p;
    AVPacket packet;

    while (running) {
        if (queue_get(packets, &packet)) {
            decode_packet(&packet, ctx);
            av_free_packet(&packet);
            continue;
        }

        if (running == 0 || (eof && packets->nb_packets == 0)) {
            break;
        }
    }

    LOGI("Audio decoding thread - has been stopped\n");
    return NULL;
}

void decode_packet(AVPacket* packet, AVCodecContext* ctx) {
//    LOGI("decoding audio\n");
}

void audio_set_parameters(void* p) {

}
