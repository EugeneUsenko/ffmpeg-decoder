/*
 * video_decoder_thread.h
 *
 *  Created on: Apr 6, 2013
 *      Author: ievgen usenko
 */

#ifndef VIDEO_DECODER_THREAD_H_
#define VIDEO_DECODER_THREAD_H_

#include "ffmpeg.h"

typedef struct {
	AVFrame* yuv;
	AVFrame* rgb;
	struct SwsContext* sws;
	void (*callback)(uint8_t*, uint32_t);
} VideoThreadParameters;

int video_start(AVCodecContext*);
void video_set_parameters(void* p);
void video_stop(void);
void video_push_packet(AVPacket*);
void video_eof();

VideoThreadParameters* create_vparameters(AVCodecContext* ctx, int width, int height);
void release_vparameters(VideoThreadParameters* params);

#endif /* VIDEO_DECODER_THREAD_H_ */
