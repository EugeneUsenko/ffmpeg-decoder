#include <assert.h>
#include "queue.h"
#include "thread_utils.h"
#include "logging.h"

static void queue_get_unsync(PacketQueue* queue, AVPacket* packet);
static void queue_put_unsync(PacketQueue* queue, AVPacket* packet);
static void queue_clear_unsync(PacketQueue* queue);

PacketQueue* queue_create() {
    PacketQueue* queue = (PacketQueue*) malloc(sizeof(PacketQueue));
    if (queue == NULL) {
        return NULL;
    }

    queue->mutex = create_mutex();
    if (queue->mutex == NULL) {
        free(queue);
        return NULL;
    }

    queue->cond = create_cond();
    if (queue->cond == NULL) {
        destroy_mutex(queue->mutex);
        free(queue);
        return NULL;
    }

    queue->nb_packets = 0;
    queue->first = NULL;
    queue->last = NULL;
    return queue;
}

void queue_release(PacketQueue* queue) {
    assert(queue != NULL);
    assert(queue->cond != NULL);
    assert(queue->mutex != NULL);

    queue_clear_unsync(queue);
    destroy_cond(queue->cond);
    destroy_mutex(queue->mutex);
    free(queue);
}

void queue_get_unsync(PacketQueue* queue, AVPacket* packet) {
    AVPacketList* head = queue->first;

    if (head) {
        queue->first = head->next;
        if (!queue->first) {
            queue->last = NULL;
        }

        queue->nb_packets--;
        *packet = head->pkt;
        av_free(head);
    } else {
        packet->data = NULL;
    }
}

void queue_put_unsync(PacketQueue* queue, AVPacket* packet) {
    if (av_dup_packet(packet) < 0) {
        LOGE("queue_put_unsync() - can't allocate memory for packet\n");
        return;
    }

    AVPacketList* item = av_malloc(sizeof(AVPacketList));
    if (!item) {
        LOGE("queue_put_unsync() - can't allocate memory for queue item\n");
        return;
    }

    item->pkt = *packet;
    item->next = NULL;

    if (queue->last) {
        queue->last->next = item;
    } else {
        queue->first = item;
    }

    queue->last = item;
    queue->nb_packets++;
}

void queue_put(PacketQueue* queue, AVPacket* packet) {
    pthread_mutex_lock(queue->mutex);
    queue_put_unsync(queue, packet);
    pthread_cond_signal(queue->cond);
    pthread_mutex_unlock(queue->mutex);
}

int queue_get(PacketQueue* queue, AVPacket* packet) {
    pthread_mutex_lock(queue->mutex);
    if (queue->nb_packets == 0) {
        pthread_cond_wait(queue->cond, queue->mutex);
    }
    queue_get_unsync(queue, packet);
    pthread_mutex_unlock(queue->mutex);
    return packet->data == NULL ? 0 : 1;
}

void queue_clear_unsync(PacketQueue* queue) {
    AVPacket packet;
    int i;
    for (i = 0; i < queue->nb_packets; i++) {
        queue_get_unsync(queue, &packet);
        av_free_packet(&packet);
    }
}

