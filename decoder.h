#ifndef decoder_h
#define decoder_h

#include <pthread.h>

#ifdef __ANDROID__
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"
#include "libavcodec/avfft.h"
#include "libavutil/avutil.h"
#else
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavcodec/avfft.h>
#include <libavutil/avutil.h>
#endif

#include "queue.h"
#include "main.h"

#define DECODER_IS_RUNNING 1
#define DECODER_IS_STOPPED 0

typedef struct {
	AVCodecContext* codec;
	PacketQueue packets;
	int index;

} Stream;

typedef struct {
	Stream stream;
	AVFrame* rgb;
	AVFrame* yuv;
	uint8_t* frame;
	uint32_t frame_size;

	void (*callback)(uint8_t*, uint32_t);

	pthread_t thread;
	pthread_mutex_t* mutex;
	pthread_cond_t* cond;
} VideoStream;

typedef struct {
	Stream stream;
	uint8_t* frame;
	uint32_t frame_size;
	uint32_t data_len;

	void (*callback)(uint8_t*, uint32_t);

	pthread_t thread;
	pthread_mutex_t* mutex;
	pthread_cond_t* cond;
} AudioStream;

typedef struct {
	AVFormatContext* format;
	struct SwsContext* sws;
	VideoStream video;
	AudioStream audio;

	uint8_t running;
	pthread_t reading_thread;

//    pthread_t decoding_thread;
//    pthread_mutex_t* decoder_mutex;
//    pthread_cond_t* decoder_cond;
} DecoderContext;

DecoderContext* decoder_start(DecoderParameters*, void (*video_callback)(uint8_t*, uint32_t),
		void (*audio_callback)(uint8_t*, uint32_t));
void decoder_stop(DecoderContext*);
int decoder_set_output_size(DecoderContext*, int width, int height); // Coming soon

#endif
