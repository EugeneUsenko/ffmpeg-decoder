/*
 * audio_decoder_thread.h
 *
 *  Created on: Apr 6, 2013
 *      Author: ievgen usenko
 */

#ifndef AUDIO_DECODER_THREAD_H_
#define AUDIO_DECODER_THREAD_H_

#include "ffmpeg.h"

int  audio_start(AVCodecContext*);
void audio_set_parameters(void*);
void audio_stop(void);
void audio_push_packet(AVPacket*);
void audio_eof();

#endif /* AUDIO_DECODER_THREAD_H_ */
