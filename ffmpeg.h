/*
 * ffmpeg.h
 *
 *  Created on: Apr 6, 2013
 *      Author: ievgen usenko
 */

#ifndef FFMPEG_H_
#define FFMPEG_H_

#ifdef __ANDROID__
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswscale/swscale.h"
#include "libavcodec/avfft.h"
#include "libavutil/avutil.h"
#include "libavutil/avconfig.h"
#else
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
//#include <libavcodec/avfft.h>
#include <libavutil/avutil.h>
#endif

#endif /* FFMPEG_H_ */
